# Regext Patterns
import re
import random
import time
import pymysql
pymysql.install_as_MySQLdb()
import MySQLdb
import pandas as pd
from sshtunnel import SSHTunnelForwarder
import datetime as dt
import os
import numpy as np

# old db
# db_host='10.48.208.3'
# server = SSHTunnelForwarder(
#     ('104.196.15', 22),
#     ssh_username="brian",
#     ssh_private_key_password='brian',
#     ssh_pkey="sys/pubcon.srv",
#     allow_agent=False,
#     remote_bind_address=(db_host, 3306)
# ) 

# db_host='10.127.16.3'
# server = SSHTunnelForwarder(
#     ('34.75.199.147', 22),
#     # ssh_username="brian",
#     # ssh_private_key_password='brian',
#     ssh_pkey="sys/id_rsa",
#     allow_agent=False,
#     remote_bind_address=(db_host, 3306)
# ) 


def fuzz_conditional_content(regx,article):
    out=article
    # ctl=0
    for m in re.finditer(regx, article):
        # ctl= ctl + 1
        matched=m.group(0)
        # print(ctl)
        # print(matched)
        # print(m.group(2))
        condition, content = m.group(2).split('::')[0] ,m.group(2).split('::')[1]

        if eval(condition):
            out=out.replace('\n{}' .format(matched),content.strip())
        else:
            out=out.replace('\n{}' .format(matched),'')    
    
    return out

def fuzz_eval_content(regx,article):
    out=article
    for m in re.finditer(regx, article):
        # ctl= ctl + 1
        matched=m.group(0)
        form=m.group(2)
        try:
            repl=str(round(eval(form.replace(',','')),2)) 
        except:
            raise('ERROR:{} - {}' .format(matched,form))
            reply='0'
        out=out.replace(matched,repl)
    
    return out

def fuzz_remove_comments(regx,article):
    out=article
    for m in re.finditer(regx, article):
        # ctl= ctl + 1
        matched=m.group(0)
        out=out.replace(matched,'')
    
    return out




def fuzz_dynamic_content(regx,article,delim):
    
    out=article
    for m in re.finditer(regx, article):
        matched=m.group(0)
        c=int(matched[0:1]) if matched[0:1].isnumeric() else 1 

        values=m.group(1).split(delim)
        lv=len(values)
        rnd_res_m=[] 

        for i in range(0,c):
            ssd=str(time.time()).split('.')[1]
            sd=int(ssd[len(ssd) -2:])
            np.random.seed(sd)
            rnd_res= np.random.choice(values) 
            # rnd_res = values[sd % len(values)]
            # print(rnd_res,'\n')
            rnd_res_m.append(rnd_res)
            values= m.group(1).split(delim) if i+1 == lv else [x for x in values if x not in [rnd_res]]
            
        m_rnd_res=('').join(rnd_res_m)
        out=out.replace(matched,m_rnd_res.strip(),1)

    return out

def fuzz_variable(regx,article,*args,**kwargs):
    out=article
    for m0 in re.finditer(regx, article):
        # print(m0.group(0),m0.group(1))
        # kvar=kwargs[m0.group(1)]
        # kvar=m0.group(0)
        try:
            kvar=kwargs[m0.group(1)]
        except:
            kvar=m0.group(0)
            pass
        finally:
            pass
        out=out.replace(m0.group(0),kvar)
    return out













# dynamic_pattern = re.compile(r"\$\[(.*?)\]")
# variable_pattern = re.compile(r"\$\{(.*?)\}")

# def s_random(line, *args, **kwargs):
#     try:
#         for m in re.finditer(dynamic_pattern, line):
#             matched=m.group(0)
#             values=m.group(1).split('|')
#             rnd_res= random.choice(values)
#             line=line.replace(matched,rnd_res)

#         for m0 in re.finditer(variable_pattern, line):
#             # print(m0.group(0),m0.group(1),eval(m0.group(1)))
#             try:
#                 kvar=kwargs[m0.group(1)]
#             except:
#                 kvar=m0.group(0)
#                 pass
#             finally:
#                 pass
#             line=line.replace(m0.group(0),kvar)

#         return(line)
#     except Exception as e:
#         print(e)
#         pass
#         return 7700


def db_insert(endpoint,title,article):
    try:
        con = MySQLdb.connect(host='192.168.1.100'
            ,port=3306
            ,user='dbadmin' 
            ,passwd='password1'
            ,db='wp_agent')  

        with con.cursor() as cursor:
            sql0="delete from post where endpoint='{}'" .format(endpoint.replace("'","''"))
            cursor.execute(sql0)
            
            sql1="insert into post values('{}','{}','{}')" .format(endpoint.replace("'","''"),title.replace("'","''"),article.replace("'","''"))
            # print(sql1)
            cursor.execute(sql1)
        con.commit()
        con.close()
        return 0
    except Exception as e:
        print(e)
        pass
        return 7710


def mock_wpa_geta(endpoint):
    con = MySQLdb.connect(host='192.168.1.100'
        ,port=3306
        ,user='dbadmin' 
        ,passwd='password1'
        ,db='wp_agent')  
    df=pd.read_sql("select * from post where endpoint='{}'" .format(endpoint.replace("'","''")),con)
    
    con.close()
    return df

def get_prev_post(gamecode,prev_date):
    pt=''
    pn=''

    # server.start()
    # port = server.local_bind_port
    # con = pymysql.connect(
    #     host='127.0.0.1', port=port, db='flawpdb', user='root',
    #     password='c08euM8ssEDni7mn', charset='utf8mb4')
    # con = pymysql.connect(
    #        host='127.0.0.1', port=port, db='fla_clouddb', user='flasqldev',
    #        password='v85$p!HJrT!fM%uMtf', charset='utf8mb4')
    con = MySQLdb.connect(host='34.74.90.4'
                        ,port=3306
                        ,user='flasqldev' 
                        ,passwd='v85$p!HJrT!fM%uMtf'
                        ,db='fla_clouddb')  


    sql= '''
    select post_title,post_name 
    from fl_posts
    where id in (
    select max(object_id)
    from cu_posts_ext
    where gamecode='{gamecode}' and  gamedate='{prev_date}'
    )
    ''' .format(gamecode=gamecode,prev_date=prev_date)
    df=pd.read_sql(sql,con)

    try:
        pt=df.iloc[0]['post_title']
        pn=df.iloc[0]['post_name']
    except:
        pass

    out=[pt,pn]

    con.close()
    # server.stop()

    return out

def add_wp_post(title,content,POSTDATE,seotitle,*args,**kwargs):
    # post processing tile,post_name and content
    title=title.replace("'","''")
    seotitle=seotitle.replace("'","''")
    stg_=title.replace('for','')
    stg_2=re.sub("[ ]+", "-", stg_)
    post_name=re.sub("[^\-0-9a-zA-Z]+", "", stg_2).lower().replace('--','-')
    content=content.replace("'","''")

    # Derive dates
    dtfrmt='%Y-%m-%d %H:%M:%S'
    POSTTIME=dt.datetime.now().strftime('%H:%M:%S')
    POSTDTTM=(' ').join([POSTDATE,POSTTIME])
    POSTDTTM_GMT_D=dt.datetime.strptime(POSTDTTM,dtfrmt) + dt.timedelta(hours=4)
    POSTDTTM_GMT=POSTDTTM_GMT_D.strftime(dtfrmt)

    # get new post id


    # server.start()
    # port = server.local_bind_port
    # con = pymysql.connect(
    #     host='127.0.0.1', port=port, db='flawpdb', user='root',
    #     password='c08euM8ssEDni7mn', charset='utf8mb4')


    # con = pymysql.connect(
    #        host='127.0.0.1', port=port, db='fla_clouddb', user='flasqldev',
    #        password='v85$p!HJrT!fM%uMtf', charset='utf8mb4')

    con = MySQLdb.connect(host='34.74.90.4'
        ,port=3306
        ,user='flasqldev' 
        ,passwd='v85$p!HJrT!fM%uMtf'
        ,db='fla_clouddb')  

    # Check if post_url already exists 
    postcheck="select id from fl_posts where post_name='{}'" .format(post_name)
    dfid=pd.read_sql(postcheck,con)

    if len(dfid) > 0:
        exid=dfid.iloc[0]['id']
        d0="delete from fl_posts where id={}" .format(exid)
        d1="delete from fl_postmeta where post_id = {}" .format(exid)
        d2="delete from fl_posts where id = {}" .format(exid)
        d3="delete from fl_term_relationships where object_id = {}" .format(exid)
        # d4="delete from cu_posts_ext where object_id = {}" .format(exid)
        # delete post with same post_name
        cur=con.cursor()
        cur.execute(d0)
        cur.execute(d1)
        cur.execute(d2)
        cur.execute(d3)
        # cur.execute(d4)
        cur.close()
    NewID = pd.read_sql("select max(ID) +1 as i from fl_posts",con).iloc[0]['i']

    Ipost='''
    insert into fl_posts values ({NewID},2,'{post_date}','{post_date_gmt}','{content}','{title}'
    ,'','publish','open','open','','{post_name}','','','{post_date}','{post_date_gmt}'
    ,'',0,'https://flalotteryresults.com/?p={NewID}' ,0,'post','',0);
    ''' .format(NewID=NewID
                ,post_date=POSTDTTM
                ,post_date_gmt=POSTDTTM_GMT
                ,title=title
                ,content=content
                ,post_name=post_name)

    # Ipost_SEO='''
    #     insert into fl_postmeta(post_id,meta_key,meta_value)
    #         select {NewID},'rank_math_title','{seotitle}'
    #         union
    #         select {NewID},'rank_math_permalink','{post_name}'
    #         union
    #         select {NewID},meta_key,meta_value
    #         from cu_rank_math_postmeta_tpt
    # ''' .format(NewID=NewID,seotitle=seotitle,post_name=post_name)

    # Ipost_ext = '''
    #     insert into cu_posts_ext
    #         select '{GC}'
    #             ,{NewID}
    #             ,'{gamedate}'
    #             ,'{numbers}'
    #             ,'{jackpot}'
    #             ,{winners}
    #             ,'{win_info}'
    # ''' .format(GC=kwargs['GC'],NewID=NewID,gamedate=POSTDATE,numbers=kwargs['numbers']
    #             ,jackpot=kwargs['jackpot'],winners=kwargs['winners'],win_info=kwargs['win_info'])
    # print(Ipost_ext)
    cur=con.cursor()
    cur.execute(Ipost)
    # cur.execute(Ipost_SEO)
    # cur.execute(Ipost_ext)
    cur.close()



    def post_term_update(term_list,term_type):
        for p in term_list:
            term=p.replace("'","''")
            stg_slug=re.sub("[ ]+", "-", term)
            slug=re.sub("[^\-0-9a-zA-Z]+", "", stg_slug).lower()

            dfTermID = pd.read_sql("select term_id as i from  fl_terms where upper(name)='{TERM}'"  .format(TERM=term),con)
            TermID = dfTermID.iloc[0]['i'] if len(dfTermID) > 0 else 0

            if TermID == 0:
                newTermID = pd.read_sql("select max(term_id) + 1 as i from fl_terms",con).iloc[0]['i']
                NewTermSQL = '''
                                insert into fl_terms values({newTermID},'{term}','{slug}',0) 
                                ''' .format(newTermID=newTermID,term=term,slug=slug)

                newTaxonomyID = pd.read_sql("select max(term_taxonomy_id) + 1 as i from fl_term_taxonomy",con).iloc[0]['i']
                NewTaxonomyIDSQL = '''
                                insert into fl_term_taxonomy values({newTaxonomyID},{newTermID},'{term_type}','',0,0)
                                ''' .format(newTaxonomyID=newTaxonomyID,newTermID=newTermID,term_type=term_type)
                
                # TODO Execute and commit sqls
                cur=con.cursor()
                cur.execute(NewTermSQL)
                cur.execute(NewTaxonomyIDSQL)
                cur.close()
                
                TaxonomyID = newTaxonomyID
            else:
                TaxonomyID = pd.read_sql("select term_taxonomy_id as i from fl_term_taxonomy where term_id={TermID}" .format(TermID=TermID),con).iloc[0]['i']


            # inser new relationship and update taxonmy_id count
            NewTermRelSQL = '''
                            insert into fl_term_relationships
                            select {NewID},{TaxonomyID},x+1
                                from (
                                select coalesce(max(term_order),0) as x
                                from fl_term_relationships
                                where object_id={NewID}
                                ) a
                            ''' .format(NewID=NewID,TaxonomyID=TaxonomyID)
            UpTermTaxonomySQL= '''
                                update fl_term_taxonomy
                                set count = count + 1
                                where term_taxonomy_id={TaxonomyID}
                                '''  .format(TaxonomyID=TaxonomyID)

            cur=con.cursor()
            cur.execute(NewTermRelSQL)
            cur.execute(UpTermTaxonomySQL)
            cur.close()

    if 'TERMS_TOADD' in kwargs:
        for term_type in kwargs['TERMS_TOADD'].keys():
            term_list=kwargs['TERMS_TOADD'][term_type]
            # print(term_type,term_list)            
            post_term_update(term_list,term_type)


    con.commit()

    con.close()
    # server.stop()

    return 0
#%%