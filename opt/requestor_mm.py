#%% 
import requests 
import num2word
from bin.scrapper import *
from bin.functions import get_prev_post
import datetime as dt

# out = [drawdate,num1,num2,num3,num4,num5,yellowball,MP,estJackpot,nextCashOpt,winstate,winners]
def fla_megaball_loader2(year,month,date):
    mm_data=dual_scrape_mm('{}-{}-{}' .format(year,month,date))
    draw_date=dt.datetime.strptime(mm_data[0][0],'%A, %B %d, %Y')
    prev_draw_date=dt.datetime.strptime(mm_data[1][0],'%A, %B %d, %Y')
    dateformat='%B %-d, %Y'
    dayformat='%A'
    POSTDATE=draw_date.strftime('%Y-%m-%d')
    MMCURDRAWDATE = draw_date.strftime(dateformat) #'Mega Millions current draw date'
    MMCURDRAWDAY = draw_date.strftime(dayformat) #'Mega Millions previous draw day eg Tuesday, Wednesday'
    MMPREVDRAWDATE = prev_draw_date.strftime(dateformat) #'Mega Millions previous draw date'
    MMPREVDRAWDAY = prev_draw_date.strftime(dayformat)  #'Mega Millions current draw day eg Tuesday, Wednesday'
    MMCURJPANNUITY = mm_data[0][8]
    MMCURJPCASH =  mm_data[0][9]
    MMPREVJPANNUITY = mm_data[1][8]
    MMPREVJPCASH = mm_data[1][9]
    MM1=mm_data[0][1]
    MM2=mm_data[0][2]
    MM3=mm_data[0][3]
    MM4=mm_data[0][4]
    MM5=mm_data[0][5]
    MB =mm_data[0][6]
    MP =mm_data[0][7]

    CURWINNO = str(mm_data[0][11])
    PREVWINNO = str(mm_data[1][11]) #'Number of Winners for the previous draw, in words (eg One, Two, Three, etc)'
    PREVWINWORD = num2word.word(mm_data[1][11])
    PREVWINSTATENO = str(len(mm_data[1][10]))#'Unique number of winning states from previous draw'
    WINSTATE1 = ''
    WINSTATE2 = ''
    WINSTATE3 = ''
    try:
        WINSTATE1 = mm_data[1][10][0]
        WINSTATE2 = mm_data[1][10][1]
        WINSTATE3 = mm_data[1][10][2]
    except:
        pass


    CURWINSTATENO = str(len(mm_data[0][10]))#'Unique number of winning states from previous draw'

    CURWINSTATE1 = ''
    CURWINSTATE2 = ''
    CURWINSTATE3 = ''
    try:
        CURWINSTATE1 = mm_data[0][10][0]
        CURWINSTATE2 = mm_data[0][10][1]
        CURWINSTATE3 = mm_data[0][10][2]
    except:
        pass


    GAMECODE=  'MIL'
    NUMBERS = '{}-{}-{}-{}-{} MB:{} {}' .format(MM1,MM2,MM3,MM4,MM5,MB,MP)
    JACKPOT = MMPREVJPANNUITY
    WINNERS  = CURWINNO
    WININFO = '{} {}-{}-{}' .format(CURWINSTATENO,CURWINSTATE1,CURWINSTATE2,CURWINSTATE3)

    previous_result=get_prev_post(GAMECODE,prev_draw_date.strftime('%Y-%m-%d'))
    MMPREVTITLE=previous_result[0]
    MMPREVURL=previous_result[1]



    TAGS='["TAGMATED","TAG2","TAG3"]'
    CATEGORIES='["Mega Millions"]'
    API_ENDPOINT = "http://192.168.1.100:8800/__art_gen"
    header = {  'WPA-TEMPLATE' : 'templates/megamillions-format3.html'
                ,'WPA-POSTDATE' : POSTDATE 
                ,'WPA-MMCURDRAWDATE'   :  MMCURDRAWDATE  
                ,'WPA-MMPREVDRAWDAY'   :  MMPREVDRAWDAY  
                ,'WPA-MMPREVDRAWDATE'  :  MMPREVDRAWDATE 
                ,'WPA-MMCURDRAWDAY'    :  MMCURDRAWDAY   
                ,'WPA-MMCURJPANNUITY'  :  MMCURJPANNUITY 
                ,'WPA-MMCURJPCASH'     :  MMCURJPCASH    
                ,'WPA-MMPREVJPANNUITY' :  MMPREVJPANNUITY
                ,'WPA-MMPREVJPCASH'    :  MMPREVJPCASH   
                ,'WPA-MM1' : MM1 
                ,'WPA-MM2' : MM2
                ,'WPA-MM3' : MM3
                ,'WPA-MM4' : MM4
                ,'WPA-MM5' : MM5 
                ,'WPA-MB'  : MB
                ,'WPA-MP'  : MP
                ,'WPA-CURWINNO':CURWINNO 
                ,'WPA-PREVWINNO':PREVWINNO
                ,'WPA-PREVWINWORD':PREVWINWORD
                ,'WPA-PREVWINSTATENO':PREVWINSTATENO 
                ,'WPA-WINSTATE1' :WINSTATE1 
                ,'WPA-WINSTATE2':WINSTATE2 
                ,'WPA-WINSTATE3':WINSTATE3 
                ,'WPA-TAGS'            :  TAGS             
                ,'WPA-CATEGORIES'      :  CATEGORIES  
                ,'WPA-MMPREVTITLE' : MMPREVTITLE
                ,'WPA-MMPREVURL' : MMPREVURL 
                ,'WPA-POSTTITLE' : 'Mega Millions $[Numbers|Winning Numbers|Results|Lottery Numbers|Lottery Results|Winning Results] $[ |for] ${MMCURDRAWDATE}'
                ,'WPA-SEOTITLE' : '%title% - $[Jackpot worth|Jackpot hits|Jackpot reaches|Jackpot] ${MMCURJPANNUITY}'
                ,'WPA-GAMECODE' : GAMECODE
                ,'WPA-NUMBERS'  : NUMBERS
                ,'WPA-JACKPOT'  : JACKPOT
                ,'WPA-WINNERS'  : WINNERS
                ,'WPA-WININFO' : WININFO

    }
    r=requests.post(API_ENDPOINT,headers=header)
    print(r.text)
#%%
fla_megaball_loader2('2020','09','04')
#%%

from datetime import timedelta, date
def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)

start_date = date(2020, 1, 1)
end_date = date(2020, 2, 29)
for single_date in daterange(start_date, end_date):
    if single_date.weekday() in  [1,4]:   
        y = single_date.strftime("%Y")
        m = single_date.strftime("%m")
        d = single_date.strftime("%d")
        # print(y,m,d)
        fla_megaball_loader2(y,m,d)


# %%

