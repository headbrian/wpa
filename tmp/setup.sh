#!/bin/bash
# https://raw.githubusercontent.com/SelfhostedPro/selfhosted_templates/OMV5/Template/template.json
# Install mysql server

sudo apt install mariadb-server -y
sudo mysql_secure_installation

sudo mysql -u root -p

CREATE DATABASE wp_agent;
CREATE USER 'dbadmin' IDENTIFIED BY 'password1';
GRANT ALL PRIVILEGES ON *.* TO 'dbadmin';
FLUSH PRIVILEGES;

sudo mysql -u dbadmin -p
use wp_agent;
SHOW GLOBAL VARIABLES LIKE 'PORT';


sudo apt install php-mysql
# Download WP CLI
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
php wp-cli.phar --info
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp



#Download and configure WP
wp core download
wp core config --dbhost=localhost --dbname=wp_agent --dbuser=pi
chmod 644 wp-config.php

wp core install --url=localhost --title="WP Agent Test" --admin_name=admin --admin_password=admin --admin_email=brian.desagun14@gmail.com

cd wp content
mkdir uploads
chgrp web uploadsc
chmod 775 uploads

