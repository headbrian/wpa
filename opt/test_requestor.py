#%% 
import requests 
import datetime as dt

template='templates/megamillions-format3.html'
     
IP='34.83.160.11:8800'
print('*** Fetching repository updates ...')
UPDATE_ENDPOINT = "http://{}/__repoupdate" .format(IP)
up=requests.post(UPDATE_ENDPOINT)
tx=up.json()['return_code']

if tx==0:
    print('*** Repositroy update success ...')
    print('*** Rendering test post ...')
    POSTDATE=dt.datetime.now().strftime('%Y-%m-%d')
    TAGS='["TEST POST TAG 1","TEST POST TAG 2","TEST POST TAG 3"]'
    CATEGORIES='["TEST POST CATEGORY"]'

    API_ENDPOINT = "http://{}/__art_gen" .format(IP)
    header = {  'WPA-TEMPLATE' : template
                ,'WPA-POSTDATE' : POSTDATE
                ,'WPA-TAGS'            :  TAGS             
                ,'WPA-CATEGORIES'      :  CATEGORIES  
                ,'WPA-POSTTITLE' : 'Test Post Title2'
                ,'WPA-SEOTITLE' : 'Test Post SEO Title'
                ,'WPA-PREVWINWORD':'Two'
                ,'WPA-PREVWINNO' : '3'
                ,'WPA-PREVWINSTATENO' : '2'
                # For CU_POSTS_EXT table
                ,'WPA-GAMECODE' : 'DIV'
                ,'WPA-NUMBERS'  : 'DIV'
                ,'WPA-JACKPOT'  : 'DIV'
                ,'WPA-WINNERS'  : '0'
                ,'WPA-WININFO' : 'DIV'

    }
    r=requests.post(API_ENDPOINT,headers=header)
    tx2=r.json()['return_code']
    if tx2==0:
        print('*** Test post Complete ...')
    else:
        print('*** Error rendering post ... ')


# %%
