#%% 
import requests 
import datetime as dt

template='templates/mm_format_09062020.html'
     
IP='34.83.160.11:8800'
print('*** Fetching repository updates ...')
UPDATE_ENDPOINT = "http://{}/__repoupdate" .format(IP)
up=requests.post(UPDATE_ENDPOINT)
tx=up.json()['return_code']

if tx==0:
    print('*** Repositroy update success ...')
    print('*** Rendering test post ...')
    POSTDATE=dt.datetime.now().strftime('%Y-%m-%d')
    TAGS='["TEST POST TAG 1","TEST POST TAG 2","TEST POST TAG 3"]'
    CATEGORIES='["Mega Millions"]'
	
	
    API_ENDPOINT = "http://{}/__art_gen" .format(IP)
    header = {  'WPA-TEMPLATE' : template
                ,'WPA-POSTDATE' 		: 	POSTDATE
                ,'WPA-CATEGORIES'		:	CATEGORIES  
                ,'WPA-TAGS'				:	TAGS             
                ,'WPA-POSTTITLE'		:	'Mega Millions Numbers September 12, 2020'
                ,'WPA-SEOTITLE'			:	'Mega Millions Numbers September 12, 2020 - FLA Lottery Results'

                ,'WPA-MMCURDRAWDATE'	:	'September 12, 2020'
                ,'WPA-MMCURDRAWDAY'		:	'Tuesday'
                ,'WPA-MMCURJPANNUITY'	:	'20 Million'
                ,'WPA-MMCURJPCASH'		:	'10 Million'

                ,'WPA-MM1'				:	'_'			#MM 1st ball
                ,'WPA-MM2'				:	'_'			#MM 2nd ball
                ,'WPA-MM3'				:	'_'			#MM 3rd ball
                ,'WPA-MM4'				:	'_'			#MM 4th ball
                ,'WPA-MM5'				:	'_'			#MM 5th ball
                ,'WPA-MB'				:	'_'			#MM Mega ball
                ,'WPA-MP'				:	'2X'		#MM Megaplier, in words
                ,'WPA-MPVALUE'			:	'2'			#MM Megaplier value, integer
				
                ,'WPA-MMPREVWINNO'		:	'0'			#MM Previous draw winners, integer
                ,'WPA-MMPREVWINWORD'	:	'no'		#MM Previous draw winners, in words
                ,'WPA-MMPREVURL'		:	'https://flalotteryresults.com/mega-millions/mega-millions-numbers-september-4-2020/'
                ,'WPA-MMPREVDRAWDATE'	:	'September 4, 2020'
                ,'WPA-MMPREVDRAWDAY'	:	'Friday'	

 #               ,'WPA-WINSTATE1'		:	'California'
 #               ,'WPA-WINSTATE2'		:	'Minnesota'
 #               ,'WPA-WINSTATE3'		:	'Nevada'
                ,'WPA-MMPREVWINSTATENO'	:	'2'

                ,'WPA-PBPREVWINNO'		:	'0'			#PB Previous draw winners, integer
                ,'WPA-PBPREVWINWORD'	:	'no'		#PB Previous draw winners, in words
                ,'WPA-PBPREVURL'		:	'https://flalotteryresults.com/powerball/powerball-numbers-september-5-2020/'	
                ,'WPA-PBPREVDRAWDATE'	:	'September 5, 2020'
                ,'WPA-PBPREVDRAWDAY'	:	'Saturday'

                ,'WPA-M5MBNO'			:	'1'			#No. of winners of MM 5 nos + MB ie jackpot
                ,'WPA-M5NO'				:	'2'			#No. of winners of MM 5 nos
                ,'WPA-M4MBNO'			:	'30'		#No. of winners of MM 4 nos + MB
                ,'WPA-M4NO'				:	'40'		#No. of winners of MM 4 nos
                ,'WPA-M3MBNO'			:	'500'		#No. of winners of MM 3 nos + MB
                ,'WPA-M3NO'				:	'600'		#No. of winners of MM 3 nos
                ,'WPA-M2MBNO'			:	'7,000'		#No. of winners of MM 2 nos + MB
                ,'WPA-M1MBNO'			:	'8,000'		#No. of winners of MM 1 nos + MB
                ,'WPA-MBNO'				:	'90,000'	#No. of winners of MB Only

                ,'WPA-M5BASEPRIZE'		:	'1'			#Value in million USD
                ,'WPA-M4MBBASEPRIZE'	:	'10000'		#Value in USD
                ,'WPA-M4BASEPRIZE'		:	'500'		#Value in USD
                ,'WPA-M3MBBASEPRIZE'	:	'200'		#Value in USD
                ,'WPA-M3BASEPRIZE'		:	'10'		#Value in USD
                ,'WPA-M2MBBASEPRIZE'	:	'10'		#Value in USD
                ,'WPA-M1MBBASEPRIZE'	:	'4'			#Value in USD
                ,'WPA-MBBASEPRIZE'		:	'2'			#Value in USD
 				
                # For CU_POSTS_EXT table
                ,'WPA-GAMECODE' : 'DIV'
                ,'WPA-NUMBERS'  : 'DIV'
                ,'WPA-JACKPOT'  : 'DIV'
                ,'WPA-WINNERS'  : '0'
                ,'WPA-WININFO' : 'DIV'

    }
    r=requests.post(API_ENDPOINT,headers=header)
    tx2=r.json()['return_code']
    if tx2==0:
        print('*** Test post Complete ...')
    else:
        print('*** Error rendering post ... ')
