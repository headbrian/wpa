#%%
from config import *
import config
from flask import Flask, redirect, url_for,render_template,request,jsonify
from bin.functions import *
# s_random, db_insert,mock_wpa_geta
import re
import subprocess as sp
import os 

app=Flask(__name__)

@app.route("/__art_gen", methods=['POST'])
def __art_gen():
    
    if request.method=='POST':
        exec('from config import *',globals())
        headers=dict(request.headers)
        TERMS_TOADD={}
        try:
            # print(DT,JPT,DAY)
            # get wpa variables from request headers
            rheads=[]
            for key in headers:
                # print(key)
                # print(key.upper()[4:],headers[key])
                if key.upper()[0:4] == 'WPA-':
                    exec("{} = '{}'" .format(key.upper()[4:],headers[key]),globals())
                    rheads.append(key.upper()[4:])
                    # locals()key.upper()[4:]= '{}' .format(headers[key])
                    # print("{} = '{}'" .format(key.upper()[4:],headers[key]))     
                if key.upper()=='WPA-TAGS':
                    TAGS=eval(headers[key])
                    TERMS_TOADD.update(post_tag=TAGS)
                if key.upper()=='WPA-CATEGORIES':
                    CATEGORY=eval(headers[key])
                    TERMS_TOADD.update(category=CATEGORY)

            # print(TERMS_TOADD)
            # print(DT,JPT,DAY)
            v=["{} = '{}'" .format(item,eval(item)) for item in set(dir(config) + rheads) if not item.startswith("__")]
            all_vars=','.join(v)

            # TODO: add multi paragraph calls

            # srandomcall='s_random(line, {})' .format(all_vars)
            fuzzvarcall='fuzz_variable(variable_pattern,repl_stage4, {})' .format(all_vars)
            # print(fuzzvarcall)
            # with open(TEMPLATE) as file_in:
            #     print(file_in)
            #     article_gen = ''
            #     for line in file_in:
            #         article_gen= article_gen +eval(srandomcall)

            file=open(TEMPLATE,'r')
            x=file.read()
            file.close()

        

            # match 1
            dynamic_paragraph_pattern_m = re.compile(r"[1-9]\$\[\[(.*[\s\S]*?)\]\]")
            repl_stage1=fuzz_dynamic_content(dynamic_paragraph_pattern_m,x,"||")
            # match 2
            dynamic_paragraph_pattern = re.compile(r"\$\[\[(.*[\s\S]*?)\]\]")
            repl_stage2=fuzz_dynamic_content(dynamic_paragraph_pattern,repl_stage1,"||")
            # match 3
            dynamic_phrase_pattern = re.compile(r"[1-9]\$\[(.*?)\]")
            repl_stage3=fuzz_dynamic_content(dynamic_phrase_pattern,repl_stage2,"|")
            # match 4
            dynamic_phrase_pattern = re.compile(r"\$\[(.*?)\]")
            repl_stage4=fuzz_dynamic_content(dynamic_phrase_pattern,repl_stage3,"|")

            # variable replacement
            variable_pattern = re.compile(r"\$\{(.*?)\}")
            repl_var1=eval(fuzzvarcall)
            # output=fuzz_variable(variable_pattern,repl_stage4,test='test variable content'
            #                                                 ,another='another test variable')

            # conditional content
            conditional_pattern = re.compile(r"\{\{(if)(.*?|[\s\S]*?)\}\}")
            repl_cond=fuzz_conditional_content(conditional_pattern,repl_var1)

            # eval content
            eval_pattern = re.compile(r"\{\{(eval\()(.*?|[\s\S]*?)\)\}\}")
            repl_eval=fuzz_eval_content(eval_pattern,repl_cond)

            # remove comments
            comment_pattern = re.compile(r"\/\/(.*?)\:\:(.*?)\n")
            article_gen=fuzz_remove_comments(comment_pattern,repl_eval)


            # Derive Post Title and SEO title
            STGPOST_TILE=fuzz_dynamic_content(dynamic_phrase_pattern,POSTTITLE,"|")
            STGSEO_TILE=fuzz_dynamic_content(dynamic_phrase_pattern,SEOTITLE,"|")

            fuzzvartitle='fuzz_variable(variable_pattern,STGPOST_TILE, {})' .format(all_vars)
            fuzzvarseotitle='fuzz_variable(variable_pattern,STGSEO_TILE, {})' .format(all_vars)

            POSTTITLE_OUT=eval(fuzzvartitle)
            SEOTITLE_OUT=eval(fuzzvarseotitle)

            # rc=db_insert(ENDPOINT,POSTTITLE_OUT,article_gen)


            # print('winners',WINNERS)
            rc=add_wp_post(POSTTITLE_OUT,article_gen,POSTDATE,SEOTITLE_OUT,TERMS_TOADD=TERMS_TOADD          
                    ,GC=GAMECODE,numbers=NUMBERS,jackpot=JACKPOT,winners=WINNERS,win_info=WININFO)            
            return jsonify(return_code = rc)
        except Exception as e:
            print(e)
            pass
            return jsonify(return_code = 700,message = e)

@app.route("/mock_wp", methods=['POST','GET'])
def mock_wp():
    endpoint=request.args.get('endpoint')
    data=mock_wpa_geta(endpoint)

    if len(data) != 0:
        article=data.iloc[0].article
        title=data.iloc[0].title
    else:
        article='<h1> Page Does not exist.</h1> '
        title='Error'

    return render_template("base.html",title=title,cont=article)  

@app.route("/__repoupdate", methods=['POST','GET'])
def repoupdate():
    out=10
    if request.method=='POST':
        print(os.getcwd())
        out = os.system("git pull --no-edit origin master")
    return jsonify(return_code = out)  


if __name__ == '__main__':
    app.run(host= '0.0.0.0',port=8800)


    
#%%
