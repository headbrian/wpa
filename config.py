# Mega Millions
POSTTITLE = 'Default Title'
SEOTITLE = 'Default Title'
MMCURDRAWDATE = 'January 1, 1900'
MMPREVDRAWDAY = 'Sunday'
MMPREVDRAWDATE = 'January 1, 1900'
MMCURDRAWDAY = 'Sunday'

#Powerball Variables for Previous draw summary
PBPREVWINNO = '0'#Powerball Previous Draw Number of Winners, integer
PBPREVWINWORD ='No'#Powerball Previous Draw Number of Winners, in words
PBPREVDRAWDATE = 'January 1, 1900'#Powerball Previous Draw Date
PBPREVDRAWDAY = 'Saturday'#Powerball Previous Draw Day
PBPREVURL = 'https://flalotteryresults.com/games/powerball/'#Powerball Previous Draw Post URL

MMCURJPANNUITY = '$20 Million'
MMCURJPCASH = '$10 Million'
MMPREVJPANNUITY = '$40 Million'
MMPREVJPCASH = '$30 Million'

MM1=1
MM2=2
MM3=3
MM4=4
MM5=5
MB = '10'
MP = '2X'

MMPREVURL = 'Mega Millions previous draw Post URL'
MMPREVTITLE = 'Mega Millions previous draw Post Title'

MMPREVWINWORD='Two'
MMPREVWINNO = 3 #'Number of Winners for the previous draw, in words (eg One, Two, Three, etc)'
MMPREVWINSTATENO = 2#'Unique number of winning states from previous draw'
CURWINNO = 0 #'Number of Winners for the current draw, in words (eg One, Two, Three, etc)'

# TAGS=['Mega Millions Drawing last ${MMCURDRAWDAY}','TAG2']
# CATEGORIES=['CAT1','CAT2']

WINSTATE1 = 'California'
WINSTATE2 = 'Minnesota'
WINSTATE3 = 'Las Vegas'

# MMINITIALJP='$20 Million'

##
M5MBNO 		= 1#Total Winners of Match 5 + Mega Ball, integer
M5NO 		= 2#Total Winners of Match 5, integer
M4MBNO 		= 30#Total Winners of Match 4 + Mega Ball, integer
M4NO		= 400#Total Winners of Match 4, integer
M3MBNO		= 500#Total Winners of Match 3 + Mega Ball, integer
M3NO		= 600#Total Winners of Match 3, integer
M2MBNO		= 7000#Total Winners of Match 2 + Mega Ball, integer
M1MBNO		= 8000#Total Winners of Match 1 + Mega Ball, integer
MBNO		= 90000#Total Winners of Mega Ball only, integer

MPVALUE		= 3
M5MBBASEPRIZE 	= MMCURJPCASH
M5BASEPRIZE 	= 1
M4MBBASEPRIZE 	= 10000
M4BASEPRIZE		= 500
M3MBBASEPRIZE	= 200
M3BASEPRIZE		= 10
M2MBBASEPRIZE	= 10
M1MBBASEPRIZE	= 4
MBBASEPRIZE		= 2
