#%%
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains
from bs4 import BeautifulSoup
import datetime as dt

timeout=20
#%%

def get_mm_data(browser):
    out=''
    try:
        winnumbers=WebDriverWait(browser,timeout).until(EC.visibility_of_element_located((By.CSS_SELECTOR, ".winningNumbersResults")))
        res=winnumbers.get_attribute('innerHTML')

        # Get Data from table
        soup = BeautifulSoup(res, 'html.parser')
        # tbl=soup.find('tbody')
        tbody=soup.find('tbody')
        tr=tbody.find_all('tr')
        for i in tr:
            i0=BeautifulSoup(str(i),'html.parser').find('tr').contents
            # print(i0)    
            if str(i0[1]).find('>5-of-5 MB<') > 0:
                game=i0[1].text
                winners=i0[2].text
                prize=i0[3].text

        # Get Winning Number
        numbersonly=browser.find_element_by_class_name("winningNumbers")
        __numbers=numbersonly.text.split('\n')[0].split('-')
        num5 = ('-').join(__numbers[:5])
        megaball = __numbers[5]
        mult=__numbers[6]
        status=numbersonly.text.split('\n')[1]

        # Draw Date
        drawdate=browser.find_elements_by_tag_name("h3")
        dd=drawdate[4].text.split('\n')[1]

        out=[num5,megaball,mult,status,game,winners,prize,dd]
    except Exception as e:
        print(e)
        pass

    return out


# year='2020'
# month='08'
# day='21'
def get_mm_data_all(year,month,day):
    link='http://flalottery.com/site/winningNumberSearch?searchTypeIn=date&gameNameIn=MEGAMILLIONS&singleDateIn={month}%2F{day}%2F{year}' .format(year=year,month=month,day=day)

    __options = Options()
    __options.headless = True
    browser = webdriver.Chrome(chrome_options=__options)
    # browser = webdriver.Chrome(executable_path='webdriver/chromedriver.exe', chrome_options=__options)
    browser.get(link)

    # Current
    curr = get_mm_data(browser)
    out=curr

    # Get Previous if we get current

    if curr != '':
        prev=browser.find_element_by_class_name("prevNextArrows")
        ActionChains(browser).click(prev).perform()
        # Previous
        prevv = get_mm_data(browser)
        out=[curr,prevv]
    
    browser.quit()

    return out


#%%

# get_mm_data_all('2020','08','21')


def scrape_mm(date):
    out=[]
    d=dt.datetime.strptime(date,'%Y-%m-%d').date()
    x=d - dt.date(1,1,1)             
    datelink = str(x /dt.timedelta(days=1)  * 864)[:-2] +'000000000'



    link='https://www.megamillions.com/Winning-Numbers/Previous-Drawings/Previous-Drawing-Page.aspx?date={date}' .format(date=datelink)
    print(link,'\n\n')
    __options = Options()
    __options.headless = True
    browser = webdriver.Chrome(chrome_options=__options)
    # browser = webdriver.Chrome(executable_path='webdriver/chromedriver.exe', chrome_options=__options)
    browser.get(link)


    try:       
        wait=WebDriverWait(browser,timeout).until(EC.visibility_of_element_located((By.CSS_SELECTOR, ".estJackpot")))
        s=browser.find_element_by_class_name("js_getNumbersByDate")
        res=s.get_attribute('innerHTML')

    except Exception as e:
        print(e)
        pass
    else:
        soup = BeautifulSoup(res, 'html.parser')
        drawdate=soup.find("h1",class_="winningNumbersHead").text
        num1=soup.find("li", class_="ball winNum1").text
        num2=soup.find("li", class_="ball winNum2").text
        num3=soup.find("li", class_="ball winNum3").text
        num4=soup.find("li", class_="ball winNum4").text
        num5=soup.find("li", class_="ball winNum5").text
        yellowball=soup.find("li", class_="ball yellowBall winNumMB").text
        MP= soup.find("span", class_="winNumMP").text 
        estJackpot=soup.find("span", class_="estJackpot js_pastJackpot").text  
        nextCashOpt=soup.find("span", class_="nextCashOpt js_pastCashOpt").text
        winstate=soup.find(id='bwJP').text.split('\n')[2].split(',')
        winners=0
        winstate_out=[]
        for st in winstate:
            mult=st.strip().find('(')
            if mult > 1:
                num=st.strip()[mult+1: st.strip().find(')')]
                winners= winners + int(num)
                winstate_out.insert(0,st.strip()[0,2])
            elif st != 'None':
                winners = winners +1
                winstate_out.append(st.strip()) 

        out = [drawdate,num1,num2,num3,num4,num5,yellowball,MP,estJackpot,nextCashOpt,winstate_out,winners]
    finally:
        browser.quit()
        return out
# %%
def dual_scrape_mm(date):
    out=[]
    focus_date=dt.datetime.strptime(date,'%Y-%m-%d')
    if focus_date.weekday() in  [1,4]:   
        out1=scrape_mm(date)
        if focus_date.weekday() ==1:
            back_date = focus_date - dt.timedelta(days=4)
        elif focus_date.weekday() ==4:
            back_date = focus_date - dt.timedelta(days=3)
        date2=back_date.strftime('%Y-%m-%d')   
        out2=scrape_mm(date2)
        out=[out1,out2]
    return out

#%%    
# date='2020-07-31'
# dual_scrape_mm(date)