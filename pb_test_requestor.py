#%% 
import requests 
import datetime as dt

template='templates/pb_template_09182020.html'
     
IP='34.83.160.11:8800'
print('*** Fetching repository updates ...')
UPDATE_ENDPOINT = "http://{}/__repoupdate" .format(IP)
up=requests.post(UPDATE_ENDPOINT)
tx=up.json()['return_code']

if tx==0:
    print('*** Repositroy update success ...')
    print('*** Rendering test post ...')
    POSTDATE=dt.datetime.now().strftime('%Y-%m-%d')
    TAGS='["TEST POST TAG 1","TEST POST TAG 2","TEST POST TAG 3"]'
    CATEGORIES='["Mega Millions"]'
	
    API_ENDPOINT = "http://{}/__art_gen" .format(IP)
    header = {  'WPA-TEMPLATE' 			: 	template
                ,'WPA-POSTDATE' 		: 	POSTDATE
                ,'WPA-CATEGORIES'		:	CATEGORIES  
                ,'WPA-TAGS'				:	TAGS             
                ,'WPA-POSTTITLE'		:	'Powerball Numbers September 23, 2020'
                ,'WPA-SEOTITLE'			:	'Powerball Numbers September 23, 2020 - FLA Lottery Results'

                ,'WPA-PBCURDRAWDATE'	:	'September 12, 2020'
                ,'WPA-PBCURDRAWDAY'		:	'Tuesday'
                ,'WPA-PBCURJPANNUITY'	:	'20 Million'
                ,'WPA-PBCURJPCASH'		:	'10 Million'

                ,'WPA-PB1'				:	'_'			#PB 1st ball
                ,'WPA-PB2'				:	'_'			#PB 2nd ball
                ,'WPA-PB3'				:	'_'			#PB 3rd ball
                ,'WPA-PB4'				:	'_'			#PB 4th ball
                ,'WPA-PB5'				:	'_'			#PB 5th ball
                ,'WPA-PB'				:	'_'			#PB Powerball
                ,'WPA-PP'				:	'X_'		#PB PowerPlay
                ,'WPA-PPVALUE'			:	'10'			#PB PowerPlay value, integer

                ,'WPA-PBPREVWINNO'		:	'1'			#PB Previous draw winners, integer
                ,'WPA-PBPREVWINWORD'	:	'no'		#PB Previous draw winners, in words
                ,'WPA-PBPREVURL'		:	'https://flalotteryresults.com/powerball/powerball-numbers-september-16-2020/'
                ,'WPA-PBPREVDRAWDATE'	:	'September 16, 2020'
                ,'WPA-PBPREVDRAWDAY'	:	'Wednesday'	

                ,'WPA-PBWINSTATE1'		:	'Texas'
                ,'WPA-PBWINSTATE2'		:	'North Dakota'
                ,'WPA-PBWINSTATE3'		:	'New York'
                ,'WPA-PBPREVWINSTATENO'	:	'2'

                ,'WPA-MMPREVWINNO'		:	'1'			#MM Previous draw winners, integer
                ,'WPA-MMPREVWINWORD'	:	'no'		#MM Previous draw winners, in words
                ,'WPA-MMPREVURL'		:	'https://flalotteryresults.com/mega-millions/mega-millions-numbers-september-15-2020/'	
                ,'WPA-MMPREVDRAWDATE'	:	'September 15, 2020'
                ,'WPA-MMPREVDRAWDAY'	:	'Tuesday'

                ,'WPA-PBM5PBNO'			:	'1'			#No. of winners of PB 5 nos + PB ie jackpot
                ,'WPA-PBM5NO'			:	'2'			#No. of winners of PB 5 nos
                ,'WPA-PBM4PBNO'			:	'30'		#No. of winners of PB 4 nos + PB
                ,'WPA-PBM4NO'			:	'40'		#No. of winners of PB 4 nos
                ,'WPA-PBM3PBNO'			:	'500'		#No. of winners of PB 3 nos + PB
                ,'WPA-PBM3NO'			:	'600'		#No. of winners of PB 3 nos
                ,'WPA-PBM2PBNO'			:	'7,000'		#No. of winners of PB 2 nos + PB
                ,'WPA-PBM1PBNO'			:	'8,000'		#No. of winners of PB 1 nos + PB
                ,'WPA-PBNO'				:	'90,000'	#No. of winners of PB Only

                ,'WPA-PBM5BASEPRIZE'	:	'1'			#Value in million USD
                ,'WPA-PBM4PBBASEPRIZE'	:	'50,000'	#Value in USD
                ,'WPA-PBM4BASEPRIZE'	:	'100'		#Value in USD
                ,'WPA-PBM3PBBASEPRIZE'	:	'100'		#Value in USD
                ,'WPA-PBM3BASEPRIZE'	:	'7'			#Value in USD
                ,'WPA-PBM2PBBASEPRIZE'	:	'7'			#Value in USD
                ,'WPA-PBM1PBBASEPRIZE'	:	'4'			#Value in USD
                ,'WPA-PBPBBASEPRIZE'	:	'4'			#Value in USD
 				
                # For CU_POSTS_EXT table
                ,'WPA-GAMECODE' : 'DIV'
                ,'WPA-NUMBERS'  : 'DIV'
                ,'WPA-JACKPOT'  : 'DIV'
                ,'WPA-WINNERS'  : '0'
                ,'WPA-WININFO' : 'DIV'
    }
    r=requests.post(API_ENDPOINT,headers=header) 
    tx2=r.json()['return_code']
    if tx2==0:
        print('*** Test post Complete ...')
    else:
        print('*** Error rendering post ... ')


# %%

# %%
