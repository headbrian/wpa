#%% 
import requests 
import num2word
from bin.scrapper import *
import datetime as dt

num2word.word(2343234)
def fla_megaball_loader(year,month,date):
    mm_data=get_mm_data_all(year,month,date)
    draw_date=dt.datetime.strptime(mm_data[0][7],'%m/%d/%Y')
    prev_draw_date=dt.datetime.strptime(mm_data[1][7],'%m/%d/%Y')
    dateformat='%B %d, %Y'
    dayformat='%A'
    POSTDATE=draw_date.strftime('%Y-%m-%d')
    MMCURDRAWDATE = draw_date.strftime(dateformat) #'Mega Millions current draw date'
    MMCURDRAWDAY = draw_date.strftime(dayformat) #'Mega Millions previous draw day eg Tuesday, Wednesday'
    MMPREVDRAWDATE = prev_draw_date.strftime(dateformat) #'Mega Millions previous draw date'
    MMPREVDRAWDAY = prev_draw_date.strftime(dayformat)  #'Mega Millions current draw day eg Tuesday, Wednesday'
    MMCURJPANNUITY = '$1XX Million'
    MMCURJPCASH =  mm_data[0][6]
    MMPREVJPANNUITY = '$1XX Million'
    MMPREVJPCASH = mm_data[1][6]
    WINNUMBERS=mm_data[0][0]
    MB = mm_data[0][1]
    MP = mm_data[0][2]
    WIN=mm_data[0][5]
    TAGS='["TAGMATED","TAG2","TAG3"]'
    CATEGORIES='["Mega Millions"]'
    API_ENDPOINT = "http://192.168.1.100:8800/__art_gen"
    header = {  'WPA-TEMPLATE' : 'templates/fla_mm2'
                ,'WPA-POSTDATE' : POSTDATE 
                ,'WPA-MMCURDRAWDATE'   :  MMCURDRAWDATE  
                ,'WPA-MMPREVDRAWDAY'   :  MMPREVDRAWDAY  
                ,'WPA-MMPREVDRAWDATE'  :  MMPREVDRAWDATE 
                ,'WPA-MMCURDRAWDAY'    :  MMCURDRAWDAY   
                ,'WPA-MMCURJPANNUITY'  :  MMCURJPANNUITY 
                ,'WPA-MMCURJPCASH'     :  MMCURJPCASH    
                ,'WPA-MMPREVJPANNUITY' :  MMPREVJPANNUITY
                ,'WPA-MMPREVJPCASH'    :  MMPREVJPCASH   
                ,'WPA-WINNUMBERS'      :  WINNUMBERS     
                ,'WPA-MB'              :  MB             
                ,'WPA-MP'              :  MP             
                ,'WPA-WIN'             :  WIN   
                ,'WPA-TAGS'            :  TAGS             
                ,'WPA-CATEGORIES'      :  CATEGORIES  
                ,'WPA-POSTTITLE' : 'Mega Millions $[Numbers|Winning Numbers|Results|Lottery Numbers|Lottery Results|Winning Results] $[ |for] ${MMCURDRAWDATE}'
                ,'WPA-SEOTITLE' : '(Same as generated above) - $[Jackpot worth|Jackpot hits|Jackpot reaches|Jackpot] ${MMCURJPANNUITY}'
    }
    r=requests.post(API_ENDPOINT,headers=header)
    print(r.text)
#%%
from datetime import timedelta, date
def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)

start_date = date(2020, 1, 1)
end_date = date(2020, 8, 29)
for single_date in daterange(start_date, end_date):
    if single_date.weekday() in  [1,4]:   
        y = single_date.strftime("%Y")
        m = single_date.strftime("%m")
        d = single_date.strftime("%d")
        # print(y,m,d)
        fla_megaball_loader(y,m,d)


# %%
fla_megaball_loader('2019','12','17')


#%%
import requests 

TAGS='["TAGMATED","TAG2","TAG3"]'
CATEGORIES='["Mega Millions"]'
API_ENDPOINT = "http://192.168.1.100:8800/__art_gen"
header = {  'WPA-TEMPLATE' : 'templates/megamillions-format2.html'
            ,'WPA-POSTDATE' : '2020-01-01' 
            ,'WPA-TAGS'            :  TAGS             
            ,'WPA-CATEGORIES'      :  CATEGORIES  
            ,'WPA-POSTTITLE' : 'Mega Millions $[Numbers|Winning Numbers|Results|Lottery Numbers|Lottery Results|Winning Results] $[ |for] ${MMCURDRAWDATE}'
            ,'WPA-SEOTITLE' : '(Same as generated above) - $[Jackpot worth|Jackpot hits|Jackpot reaches|Jackpot] ${MMCURJPANNUITY}'
}
r=requests.post(API_ENDPOINT,headers=header)
print(r.text)


#%%
import datetime as dt
date='2020-08-04'
d=dt.datetime.strptime(date,'%Y-%m-%d').date()
x=d - dt.date(1,1,1)             
print(str(x /dt.timedelta(days=1)  * 864)[:-2] +'000000000')
